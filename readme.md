# Description

This Rust program can be used to determine whether you are on track to read a book a week in the current year.

It was originally started as a 'my first Rust program.'


# Building and Running

To build and run:
1. Install Rust
2. Change into the right directory: `cd general.librarything`
3. Build only: `cargo build`
4. Or to build and run: `cargo run`


# Next Steps

- Split the code up into multiple functions
- [DONE] Read userid and key from file:
    [settings]
    userid = AndrewFairley
    key = <something>
- [DONE] Including generating the settings file from user input if it doesn't exist yet

- [DONE] Reduce the file size

- Update cmdline options to take a year

- Split the single function up into:
    - function to query LibraryThing
    - function to count books for a given year
    - function to report results

use chrono::{Datelike};
use chrono::prelude::*;
use colored::*;
use reqwest;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::{Value};
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::fs;
use std::io::{self, BufRead};
use std::io::Read;
use std::path::Path;

#[derive(Serialize, Deserialize)]
struct Book {
    // There are other fields, but we can ignore them
    book_id: String,
    title: String,
    author_fl: String,
    entry_date: String,
}

#[derive(Serialize, Deserialize)]
struct LibraryThingResult {
    settings : HashMap<String, Value>,
    books : HashMap<String, Book>,
}

#[derive(Serialize, Deserialize)]
struct Settings {
    userid : String,
    key : String,
}


fn get_settings() -> (Settings) {
    let cfg_filename = ".librarything.json";

    let mut settings = Settings {
        userid : String::new(),
        key : String::new(),
    };

    if !Path::new(cfg_filename).exists() {
        // Get userID and key
        println!("Enter LibraryThing userID:");
        io::stdin().lock().read_line(&mut settings.userid).unwrap();

        println!("Enter LibraryThing key:");
        io::stdin().lock().read_line(&mut settings.key).unwrap();

        let encoded_settings = serde_json::to_string(&settings).unwrap_or_else(|e| {
            panic!("Failed to serialize Settings, error is {}", e);
        });

        fs::write(cfg_filename, encoded_settings).expect("Unable to write file");
    } else {
        let encoded_settings = fs::read_to_string(cfg_filename).expect("Unable to read from config file");

        settings = serde_json::from_str(&encoded_settings).unwrap_or_else(|e| {
            panic!("Failed to parse json, error is {}", e);
        });
    }

    settings
}


fn get_books(settings : Settings) -> (LibraryThingResult) {
    let request_url = format!("http://www.librarything.com/api_getdata.php?userid={userid}&key={key}&booksort=entry_REV&resultsets=books&max=1000",
                                userid = settings.userid,
                                key = settings.key);

    let mut resp = reqwest::get(&request_url)
        .expect("Failed to get books from LibraryThing");
    let mut body = String::new();
    resp.read_to_string(&mut body).expect("Failed to read response");

    body = str::replace(&body, "var widgetResults = ", "");
    body = str::replace(&body, ";LibraryThing.bookAPI.displayWidgetContents(widgetResults, \"LT_Content\");", "");

    let json: LibraryThingResult = serde_json::from_str(&body).unwrap_or_else(|e| {
        panic!("Failed to parse json, error is {}", e);
    });

    json
}


fn generate_report(bookinfo : LibraryThingResult) {
    let now = Local::now();
    let cmpyear : String = now.format("%Y").to_string();
    let mut num_books = 0;
    let mut _books_by_year : BTreeMap<u64,u64> = BTreeMap::new();

    for (_book_id, _book_details) in &bookinfo.books {
        //println!("{}", bookID);
        //println!("{}", bookDetails.title);

        let year_regex = Regex::new(
            r".*(?P<y>\d{4})"
            ).unwrap();
        let year = year_regex.replace_all(&_book_details.entry_date, "$y");
        //println!("{}", year);

        let _year_num : u64 = year.parse().expect("Year isn't a number");

        *_books_by_year.entry(_year_num).or_insert(0) += 1;

        if year == cmpyear {
            num_books += 1;
        }
    }

    println!("{}", "Books read:".white().bold());
    for (k, x) in _books_by_year {
        println!("[{}] {}", k.to_string().blue().bold(), x.to_string().yellow().bold());
    }
    println!("");

    let week_num = now.iso_week().week();
    match num_books.cmp(&week_num) {
        Ordering::Less => println!("You're {} books BEHIND.", (week_num-num_books).to_string().red().bold()),
        Ordering::Greater => println!("You're {} books ahead.", (num_books-week_num).to_string().green().bold()),
        Ordering::Equal => println!("You're on track!"),
    }

    println!("{}: {}", "Number of books read this year".white().bold(), num_books.to_string().yellow().bold());
    println!("{}: {week_num:>width$}", "Week Number".white().bold(), week_num=week_num.to_string().yellow().bold(), width=21);
}


fn main() -> reqwest::Result<()> {

    let settings = get_settings();

    let bookinfo = get_books(settings);

    generate_report(bookinfo);

    Ok(())
}
